(defproject guess-the-word "0.1.0-SNAPSHOT"
  :description "Guess the word game to be played on command line"
  :license {:name "MIT license"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :main ^:skip-aot guess-the-word.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
