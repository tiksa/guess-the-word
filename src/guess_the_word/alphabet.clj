(ns guess-the-word.alphabet)

(def alphabet (clojure.string/split (slurp "dict.txt") #"\n"))

(defn rand-word
  []
  (let [len (count alphabet)]
    (nth alphabet (rand-int len))))