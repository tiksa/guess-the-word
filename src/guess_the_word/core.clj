(ns guess-the-word.core
  (:use [guess-the-word.alphabet :as alphabet])
  (:gen-class))

(def word (map #(assoc {} :letter % :guessed false) (alphabet/rand-word)))

(def state {:word word :guesses 10 :used-letters #{}})

(defn dec-guesses
  [state]
  (assoc state :guesses (dec (:guesses state))))

(defn dec-guesses-if-needed
  [state c]
  (if (some #(= % c) (map #(:letter %) (:word state)))
    state
    (dec-guesses state)))

(defn assoc-word
  [state word]
  (assoc state :word word))

(defn guess
  [state c]
  (assoc-word (dec-guesses-if-needed state c) (map 
    #(if (= c (:letter %)) (assoc % :guessed true) %)
    (:word state))))

(defn add-used-letter
  [state c]
  (assoc state :used-letters (conj (:used-letters state) c)))

(defn hidden-form
  [word]
  (apply str 
     (map #(if (:guessed %)
          (:letter %)
          "_") word)))

(defn visible-form
  [word]
  (apply str (map #(:letter %) word)))

(defn print-state
  [state]
  (println (str 
             (hidden-form (:word state))
             "   Guesses left: " (:guesses state)
             "   Used letters: " (apply str (:used-letters state)))))

(defn all-guessed?
  [word]
  (every? #(true? (:guessed %)) word))

(defn read-letter
  []
  (loop []
    (let [s (read-line)]
      (cond
        (empty? s) (recur)
        (> (count s) 1) (recur)
        :else (first (clojure.string/lower-case (first s)))))))

(defn -main
  [& args]
  (loop [state state
         running true]
    (cond
      (not running) nil
      (all-guessed? (:word state)) (do
                                     (print-state state)
                                     (println "You win!")
                                     nil)
      (zero? (:guesses state)) (do
                                 (print-state state)
                                 (println (str
                                            "You lose! Correct answer: "
                                            (visible-form (:word state)))))
      :else (do
              (print-state state)
              (let [c (read-letter)]
	            (recur
                  (add-used-letter (guess state c) c)
                  true))))))