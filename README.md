# guess-the-word

The traditional Guess-the-word game to be played on command line

## Installation

$ ./package.sh

## Usage

$ java -jar guess-the-word-0.1.0-standalone.jar

## License

Copyright © 2015 Timo Saarinen

Distributed under the MIT License.
